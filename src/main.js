import Vue from 'vue'
import axios from 'axios'
import VueSocketio from 'vue-socket.io-extended'
import io from 'socket.io-client'
import Antd from 'ant-design-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import config from './config/api.json'
import 'ant-design-vue/dist/antd.css'

const API_URL = config.base_url
const API_PORT = config.api_port
const API_KEY = config.aceleague_api_key
axios.defaults.baseURL = API_URL
axios.defaults.headers.post['X-Api-Key'] = API_KEY
axios.defaults.headers.get['X-Api-Key'] = API_KEY

Vue.config.productionTip = false

Vue.use(VueSocketio, io(`${API_URL}:${API_PORT}`))
Vue.use(Antd)

/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
