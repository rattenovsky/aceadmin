import axios from 'axios'
import config from '@/config/api.json'

export default() => axios.create({
  baseURL: config.base_url,
  withCredentials: true,
  headers: {
    'X-Api-Key': config.aceleague_api_key
  }
})
