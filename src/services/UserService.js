import Api from '@/services/Api'
import queryString from 'query-string'

export default {
  getUser (params) {
    return Api().get(`/api/user/${params.username}`)
  },
  getUsers () {
    return Api().get('/api/users')
  },
  loginUser (params) {
    return Api().post('/api/loginuser', queryString.stringify(params))
  },
  registerUser (params) {
    return Api().post('/api/registeruser', queryString.stringify(params))
  },
  getFriends (params) {
    return Api().get('/api/getfriends')
  },
  getSession (params) {
    return Api().get('/api/getsession')
  },
  getProfileFriends (params) {
    return Api().get(`/api/getfriends/${params.username}`)
  },
  getOnlineTimers (params) {
    return Api().get('/api/getLastOnlineTimes/', {
      params: {
        friends: params.reduce((arr, elem) => arr.concat(elem.username), [])
      }
    })
  },
  getMessages (params) {
    return Api().get(`/api/getMessages/${params.from}/${params.to}`)
  },
  keepAlive () {
    return Api().get('/api/keepalive/')
  },
  sendMessage (payload) {
    return Api().post('/api/addmessage', queryString.stringify(payload))
  },
  markMessagesAsRead (payload) {
    return Api().post('/api/markmessagesasread', queryString.stringify(payload))
  },
  markNotificationsAsRead (payload) {
    return Api().post('/api/markNotificationsAsRead', queryString.stringify(payload))
  },
  sendFriendship (payload) {
    return Api().post('/api/sendFriendship', queryString.stringify(payload))
  },
  acceptFriendship (payload) {
    return Api().post('/api/acceptFriendship', queryString.stringify(payload))
  },
  removeFriendship (payload) {
    return Api().post('/api/removeFriendship', queryString.stringify(payload))
  },
  changeAvatar (payload) {
    const data = new FormData()
    data.append('username', payload.user.username)
    data.append('file', payload.file)
    return Api().post('/api/changeAvatar', data)
  },
  connectLolAccount (payload) {
    return Api().post('/api/connectLol', queryString.stringify(payload))
  }
}
