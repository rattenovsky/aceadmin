import SecureLS from 'secure-ls'
import router from '@/router/index'
import UserService from '@/services/UserService'
import { message } from 'ant-design-vue'
const sls = new SecureLS({
  encodingType: 'rabbit',
  isCompression: false
})
const state = {
  isLoggedIn: sls.get('User').data === null,
  users: [],
  user: null
}
const mutations = {
  LOGIN_SUCCESS (state, payload) {
    state.isLoggedIn = true
    state.user = payload
    router.push('/')
    message.success('Logowowanie powiodło się')
  },
  LOGOUT (state) {
    state.user = null
    state.isLoggedIn = false
    router.push('/')
    message.success('Zostałeś poprawnie wylogowany')
  }
}
const actions = {
  login ({ commit }, payload) {
    const loginData = {
      login: payload.login || null,
      password: payload.password || null
    }
    UserService.loginUser(loginData).then(res => {
      if (res.data) {
        if (res.data.status === 'Success') {
          commit('LOGIN_SUCCESS', res.data.user)
        } else {
          message.error('Logowanie nie powiodło się')
        }
      } else {
        message.error('An error occured')
      }
    })
  },
  logout ({ commit }) {
    commit('LOGOUT')
  }
}
const getters = {
  isLoggedIn: state => state.isLoggedIn,
  user: state => state.user
}
export default {
  state,
  mutations,
  actions,
  getters
}
