import Vue from 'vue'
import Vuex from 'vuex'
import VueLodash from 'vue-lodash'
import SecureLS from 'secure-ls'
import createPersistedState from 'vuex-persistedstate'
import createLogger from 'vuex/dist/logger'
import VueNoty from 'vuejs-noty'
import modules from './modules'

Vue.use(Vuex)
Vue.use(VueLodash)
Vue.use(VueNoty, {
  timeout: 4000,
  progressBar: false,
  layout: 'bottomRight',
  theme: 'metroui',
  container: '#notyContainer',
  visibilityControl: false,
  animation: {
    open: 'animated fadeIn',
    close: 'animated fadeOut'
  }
})

const sls = new SecureLS({
  encodingType: 'rabbit',
  isCompression: false
})
const plugins = []
plugins.push(createPersistedState({
  key: 'dataCenter',
  storage: {
    getItem: key => sls.get(key),
    setItem: (key, value) => sls.set(key, value),
    removeItem: key => sls.remove(key)
  }
}))
if (process.env.NODE_ENV !== 'production') {
  plugins.push(createLogger({
    collapsed: false,
    filter (mutation) {
      return mutation.type !== 'UPDATE_FRIENDS_TIMERS'
    }
  }))
}
export default new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== 'production',
  plugins
})
